# ML-Challenge

This is the Beyond Exams - ML Challenge

## ScreenShots of Postman Testing

- ### Search(*Gives information about a YouTube video, channel, or playlist*)
![search_image](images/search_postman_1.png)

- ### Video Details(*Gives all the details of a particular Video*)
![details_image](images/post_video_details.png)
    

## Team Members
- [Rohan Babbar](https://gitlab.com/rohanbabbar04) - rohanbabbar0408@gmail.com
- [Manu Dev](https://gitlab.com/manudev0004) - manudev0004@gmail.com
- [Shivam Kumar](https://gitlab.com/0512-Shivam) - shivamkumardss2018@gmail.com
- [Abhinav Kumar](https://gitlab.com/abhinav3584) - dead.ground.75000@gmail.com